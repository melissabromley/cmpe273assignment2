package application.controller;

import application.model.ModeratorRepository;
import application.model.Poll;
import application.model.PollRepository;
import application.view.View;
import com.fasterxml.jackson.annotation.JsonView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping(value="api/v1/polls")
public class PollController {

    @Autowired
    private ModeratorRepository moderatorRepository;

    @Autowired
    private PollRepository pollRepository;

    @RequestMapping(value="{poll_id}",
            method= RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    @JsonView(View.PollViewWithoutResults.class)
    public Poll viewPollWithoutResult(@PathVariable String poll_id) {
        return pollRepository.findById(poll_id);
    }

    @RequestMapping(value="{poll_id}",
            method=RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void vote(@NotNull @PathVariable String poll_id,
                     @RequestParam(value="choice", required=true) int choice_index) {
        Poll poll = pollRepository.findById(poll_id);
        poll.vote(choice_index);
        pollRepository.save(poll);
    }
}
