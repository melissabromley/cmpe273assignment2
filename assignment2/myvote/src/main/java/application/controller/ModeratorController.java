package application.controller;

import application.model.*;
import application.view.View;
import com.fasterxml.jackson.annotation.JsonView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.concurrent.atomic.AtomicInteger;

@RestController
@RequestMapping(value="api/v1/moderators")
public class ModeratorController {
    private static AtomicInteger moderatorId = new AtomicInteger(1);
    private static AtomicInteger pollId = new AtomicInteger(1);

    @Autowired
    private ModeratorRepository moderatorRepository;

    @Autowired
    private PollRepository pollRepository;

    @RequestMapping(value="",
            method= RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public Moderator createModerator(@Valid @RequestBody Moderator moderator) {
        String new_id = Long.toString(moderatorId.getAndIncrement(), 36);
        moderator.setId(new_id);
        String createdAt = new SimpleDateFormat(
                "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").format(
                Calendar.getInstance().getTime());
        moderator.setCreated_at(createdAt);
        moderatorRepository.save(moderator);
        return moderator;
    }

    @RequestMapping(value="{moderator_id}",
            method=RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public Moderator viewModerator(@PathVariable String moderator_id) {
        return moderatorRepository.findById(moderator_id);
    }

    @RequestMapping(value="{moderator_id}",
            method=RequestMethod.PUT)
    @ResponseStatus(HttpStatus.CREATED)
    @Valid @ResponseBody
    public Moderator updateModerator(@PathVariable String moderator_id,
                                     @Valid @RequestBody Credentials credentials) {
        Moderator mod = moderatorRepository.findById(moderator_id);
        mod.setEmail(credentials.getEmail());
        mod.setPassword(credentials.getPassword());
        moderatorRepository.save(mod);
        return mod;
    }

    @RequestMapping(value="{moderator_id}/polls",
            method=RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @JsonView(View.PollViewWithoutResults.class)
    @ResponseBody
    public Poll createPoll(@PathVariable String moderator_id,
                           @Valid @RequestBody Poll poll) {
        String new_id = Long.toString(pollId.getAndIncrement(), 36);
        poll.setId(new_id);
        poll.setModeratorId(moderator_id);
        Integer [] results = new Integer[poll.getChoice().length];
        for(int i = 0; i < results.length; i++) {
            results[i] = 0;
        }
        poll.setResults(results);
        pollRepository.save(poll);
        return poll;
    }

    @JsonView(View.PollViewWithResults.class)
    @RequestMapping(value="{moderator_id}/polls/{poll_id}",
            method=RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public Poll viewPollWithResult(@PathVariable int moderator_id,
                                   @PathVariable String poll_id) {
        return pollRepository.findById(poll_id);
    }

    @JsonView(View.PollViewWithResults.class)
    @RequestMapping(value="{moderator_id}/polls",
            method=RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public Collection<Poll> listAllPolls(@PathVariable String moderator_id) {
        return pollRepository.findByModeratorId(moderator_id);
    }

    @RequestMapping(value="{moderator_id}/polls/{poll_id}",
            method=RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletePoll(@PathVariable int moderator_id,
                           @PathVariable String poll_id) {
        pollRepository.delete(poll_id);
    }
}
