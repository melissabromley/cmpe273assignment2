package application;

import application.model.ModeratorRepository;
import application.model.PollRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@ComponentScan
@EnableAutoConfiguration
@SpringBootApplication
@EnableScheduling
public class Application extends WebMvcConfigurerAdapter{
    @Autowired
    private static ModeratorRepository moderatorRepository;

    @Autowired
    private static PollRepository pollRepository;

    public static void main(String [] args) {
        SpringApplication.run(Application.class, args);
    }

    public static ModeratorRepository getModeratorRepository() {
        return moderatorRepository;
    }

    public static PollRepository getPollRepository() {
        return pollRepository;
    }
}
