package application.model;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface ModeratorRepository extends MongoRepository<Moderator, String> {

    public Moderator findById(String id);

}