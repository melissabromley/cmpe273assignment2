package application.model;

import org.hibernate.validator.constraints.NotEmpty;

public class Credentials {

    @NotEmpty
    private String email;

    @NotEmpty
    private String password;

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
