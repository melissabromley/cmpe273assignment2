package application.model;

import application.view.View;
import com.fasterxml.jackson.annotation.JsonView;
import org.hibernate.validator.constraints.NotEmpty;

public class Poll {

    @JsonView(View.PollViewWithoutResults.class)
    private String id;

    private String moderatorId;

    @NotEmpty
    @JsonView(View.PollViewWithoutResults.class)
    private String question;

    @NotEmpty
    @JsonView(View.PollViewWithoutResults.class)
    private String started_at;

    @NotEmpty
    @JsonView(View.PollViewWithoutResults.class)
    private String expired_at;

    @NotEmpty
    @JsonView(View.PollViewWithoutResults.class)
    private String [] choice;

    @JsonView(View.PollViewWithResults.class)
    private Integer [] results;

    public Poll() {}

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getModeratorId() {
        return this.moderatorId;
    }

    public void setModeratorId(String moderatorId) {
        this.moderatorId = moderatorId;
    }

    public String getQuestion() {
        return this.question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getStarted_at() {
        return this.started_at;
    }

    public void setStarted_at(String startedAt) {
        this.started_at = startedAt;
    }

    public String getExpired_at() {
        return this.expired_at;
    }

    public void setExpired_at(String expired_at) {
        this.expired_at = expired_at;
    }

    public String [] getChoice() {
        return this.choice;
    }

    public void setChoice(String [] choice) {
        this.choice = choice;
    }

    public Integer [] getResults() {
        return this.results;
    }

    public void setResults(Integer [] results) {
        this.results = results;
    }

    public void vote(int choice_index) {
        results[choice_index] = results[choice_index] + 1;
    }
}