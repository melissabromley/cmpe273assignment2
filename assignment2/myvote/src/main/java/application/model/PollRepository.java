package application.model;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface PollRepository extends MongoRepository<Poll, String> {

    public Poll findById(String id);

    public List<Poll> findByModeratorId(String id);

}