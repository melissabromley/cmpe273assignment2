package application.view;

public class View {
    public interface PollViewWithoutResults {};

    public interface PollViewWithResults extends PollViewWithoutResults {};
}
