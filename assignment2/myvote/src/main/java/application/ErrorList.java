package application;

import java.util.ArrayList;

public class ErrorList {
    private ArrayList<String> errors;

    public ErrorList() {
        this.errors = new ArrayList<String>();
    }

    public ArrayList<String> getErrors() {
        return this.errors;
    }

    public void addError(String error) {
        this.errors.add(error);
    }
}
