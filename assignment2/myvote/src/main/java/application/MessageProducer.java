package application;

import application.model.ModeratorRepository;
import application.model.Poll;
import application.model.PollRepository;
import kafka.javaapi.producer.Producer;
import kafka.producer.KeyedMessage;
import kafka.producer.ProducerConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Properties;

@Component
public class MessageProducer {
    private final String sjsuId = "009427314";
    private final String moderator = "melissa.bromley@sjsu.edu";

    private static Producer<String, String> producer;

    @Autowired
    private PollRepository pollRepository;
    private ModeratorRepository moderatorRepository;

    public MessageProducer() {
        Properties props = new Properties();
        props.put("metadata.broker.list", "54.149.84.25:9092");
        //props.put("metadata.broker.list", "localhost:9092");
        props.put("serializer.class", "kafka.serializer.StringEncoder");
        props.put("request.required.acks", "1");
        ProducerConfig config = new ProducerConfig(props);
        Producer<String, String> producer = new Producer<String, String>(config);

        this.producer = producer;
    }

    @Scheduled(fixedRate = 300000)
    public void sendMessage() {
        List<Poll> polls = pollRepository.findAll();
        long currentTime = System.currentTimeMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

        for(Poll poll : polls) {
            try {
                long expiredAt = sdf.parse(poll.getExpired_at()).getTime();
                if (expiredAt < currentTime) {
                    String[] choices = poll.getChoice();
                    Integer[] results = poll.getResults();
                    String pollResult = "Poll Result:[";
                    for(int i = 0; i < choices.length-1; i++) {
                        String choice = choices[i];
                        Integer result = results[i];
                        pollResult += choice + "=" + result + ",";
                    }
                    pollResult += choices[choices.length-1] + "=" + results[results.length-1] + "]";
                    String msg = moderator + ":" + sjsuId + ":" + pollResult;
                    KeyedMessage<String, String> data = new KeyedMessage<String, String>("cmpe273-topic", msg);
                    producer.send(data);
                }
            } catch(ParseException ex) {
                System.err.println(ex.getMessage());
            }
        }
    }
}
